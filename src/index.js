import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, browserHistory, applyRouterMiddleware } from 'react-router'
import { useScroll } from 'react-router-scroll'

import App from './App'
import Home from './pages/Home'
import About from './pages/About'

import CV from './pages/CV'
import Contact from './pages/Contact'
import Thanks from './pages/Thanks'
import Work from './pages/Work'
import Services from './pages/Services'

import Project1 from './pages/projects/Project1'
import Project2 from './pages/projects/Project2'
import Project3 from './pages/projects/Project3'
import Project4 from './pages/projects/Project4'
import Project5 from './pages/projects/Project5'
import Project6 from './pages/projects/Project6'
import Project7 from './pages/projects/Project7'
import Project8 from './pages/projects/Project8'
import DigitalWonder  from './pages/projects/digitalwonder/'

render((
  <Router
    history={browserHistory}
    render={applyRouterMiddleware(useScroll())}>
    <Route path={'/'} component={App}>
      <IndexRoute component={Home} />
      <Route path='/about' component={About} />
        <Route path='/about/cv' component={CV} />
      <Route path='/services' component={Services} />
      <Route path='/contact' component={Contact} />
      <Route path='/work' component={Work} />
        <Route path='/work/project1' component={Project1} />
        <Route path='/work/project2' component={Project2} />
        <Route path='/work/project3' component={Project3} />
        <Route path='/work/project4' component={Project4} />
        <Route path='/work/project5' component={Project5} />
        <Route path='/work/project6' component={Project6} />
        <Route path='/work/project7' component={Project7} />
        <Route path='/work/project8' component={Project8} />
     

        
{/* my section of stuff */}


        <Route path='/work/digitalwonder' component={DigitalWonder} />
     
      <Route path='/thanks' component={Thanks} />
      <Route path='*' component={Home} />
    </Route>
  </Router>
), document.getElementById('app'));

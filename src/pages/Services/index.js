import React, { Component, PropTypes } from 'react'

// import { color } from '../../vars'

import { bgImg } from '../../vars'

import Hero from '../../components/Hero'
import Container from '../../components/Container'
import Column from '../../components/Column'
import AnchorLink from '../../components/AnchorLink'
import Services from './index'
import AboutContent from './content.md'
import BlockFeature from '../../components/BlockFeature'
import ListItem from '../../components/ListItem'

class Service extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: bgImg.bg1,
        secondary: bgImg.bg2
      }
    }
    document.body.style.backgroundImage = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
    // document.body.style.backgroundColor = "green";
    document.body.style.backgroundColor = '#fff'
    document.body.style.color = '#222'
  }

  render() {
    return (
      <div>
        <Hero headline='Services.' />
        <Container>
        <Column
          width='full'
          display='flex'>
          <BlockFeature
            width='third'
            image='images/eye-icon.svg'
            title='Print, branding &amp; graphic design'
            summary='Modern design mixed with the theory of traditional graphic design.' />
          <BlockFeature
            width='third'
            nthChild='odd'
            image='images/tri-icon.svg'
            title='Application &amp; website design'
            summary='User-centered website and application design. Using Agile methods to produce quick and effective results.' />
          <BlockFeature
            width='third'
            image='images/code-icon.svg'
            title='Full-stack development'
            summary='The entire process from rapid prototyping, visual concepts to a fully functional live product.' />
        </Column>
      </Container>
      
      </div>
    )
  }
}

export default Service

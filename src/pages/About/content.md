I’m Sean, a freelance developer, engineer and designer from Hove. I love visuals as much as algorithms and I like to take a holistic approach to my work. I like using Javascript, Node.js, PHP, Python, Java and C# to build amazing user experiences.

I love using design, code and engineering to find creative solutions for
technical problems, creating awesome digital products, be it SAAS apps, CRMs, marketing/ promotional websites or mobile apps.

You can hire me directly for one of your projects or you can go through one of my companies depending on what your business requirements are: <br/><br/>
 
**Digital Wonderland Agency**: Web Design and Development - primarily for marketing and promotional websites  <br/><br/>
**AetherCloud**: ready-made and custom-made software and SAAS apps to help your business 10x its productivity  <br/><br/>
**Neon Vzn**: Mobile app development with a particular focus on augmented reality and virtual reality  <br/><br/>
**HypeNerd**: UX, SEO and Digital Marketing - providing business consultancy and project management for your digital marketing needs  <br/><br/> 
I can also help you if you have any projects that involve Data Science, Machine Learning and AI. Please contact me directly at contact@helloseanking.co.uk  <br/><br/>

## Main Languages  
Javascript  
Php  
Python  
C#  
Java  

## Main Frameworks/ Libraries/ CMS  
React, Vue, Angular  
Laravel, .Net, Spring, Node.js  
Graphql  
Processing/ p5  
WordPress/ October/ Piranha/ Custom Built CMS  
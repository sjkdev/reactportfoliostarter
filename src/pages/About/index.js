import React, { Component, PropTypes } from 'react'

// import { color } from '../../vars'

import { bgImg } from '../../vars'

import Hero from '../../components/Hero'
import Container from '../../components/Container'
import Column from '../../components/Column'
import AnchorLink from '../../components/AnchorLink'

import AboutContent from './content.md'

class Work extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: bgImg.bg1,
        secondary: bgImg.bg2
      }
    }
    document.body.style.backgroundImage = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
  }

  render() {
    return (
      <div>
        <Hero headline='Dev Nerd. Design Nerd. Math Nerd. Citizen Science Nerd. Likes MTG, skateboards and clicky pencils. ' />
        <Container>
          {/* <Column width='half'>
            <AnchorLink link='about/cv' text='Curriculum Vitae' margin='bottom' />
          </Column> */}
          <Column width='full'>
            <div dangerouslySetInnerHTML={{ __html: AboutContent }} />
          </Column>
        </Container>
      </div>
    )
  }
}

export default Work

import React, { Component, PropTypes } from 'react'
import Radium, { Style } from 'radium';

import { color } from '../../vars'

import { bgImg } from '../../vars'

import Hero from '../../components/Hero'
import Container from '../../components/Container'
import Column from '../../components/Column'
import BlockFeature from '../../components/BlockFeature'
import ListItem from '../../components/ListItem'
import AnchorLink from '../../components/AnchorLink'


class Home extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: bgImg.bg1,
        secondary: bgImg.bg2
      }
    }

    
    document.body.style.backgroundImage = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
    document.body.style.backgroundColor = '#fff'
    document.body.style.color = '#222'
  }

  render() {

    const heroParagraph = {
      fontSize: '1em',
      fontWeight: '300',
      textAlign: 'left',
      lineHeight: '200%',
      width: '85%',
      margin: '0 auto',
      color: '#444',
      '@media screen (max-width: 992px)':{
        width: '90%'
      }
    };

    const pStyle = {
      width:'100%',
      // float: 'left',
      backgroundColor: '#222',
      padding: '20px',
      color: 'white',
      fontSize: '1.2rem',
      paddingBottom: '40px'
    };

    const pStyle2 = {
      width:'100%',
      // float: 'right',
      border: 'solid 4px #222',
      padding: '20px',
      color: '#222',
      fontSize: '1.2rem',
      fontWeight: '400'
    };

    return (     
      <div>
      
        <Hero headline="Hello! I'm Sean, a Freelance Developer and Designer from Hove." link='/contact' text='Available for freelance work' />
      
        <div>
        <h4 style={heroParagraph}>
        <div>
        <p style={pStyle}>
        I design and build things in a way that are device and framework agnostic, 
        ensuring that user experience and business requirements come first and foremost. 
        I like to use Javascript, Python, C#, PHP and other cool stuff.
        </p>
        <p style={pStyle2}>
        You can hire me directly for one of your projects or you can go through one of my companies depending on what your business requirements are: <br/><br/>
 
        <b>Digital Wonderland Agency</b>: Web Design and Development - primarily for marketing and promotional websites  <br/><br/>
        <b>AetherCloud</b>: ready-made and custom-made software and SAAS apps to help your business 10x its productivity  <br/><br/>
        <b>Neon Vzn</b>: Mobile app development with a particular focus on augmented reality and virtual reality  <br/><br/>
        <b>HypeNerd</b>: UX, SEO and Digital Marketing - providing business consultancy and project management for your digital marketing needs  

        </p>
        </div>
           
        
        </h4>
        </div>
      
        
      </div>
    )
  }
}

export default Home

import React, { Component, PropTypes } from 'react'
import Radium from 'radium'

import { color, type } from '../../vars'

import Hero from '../../components/Hero'
import Container from '../../components/Container'
import Column from '../../components/Column'
import Anchor from '../../components/Anchor'

import IntroContent from './intro.md'
import PebbleContent from './pebble.md'
import FreelanceContent from './freelance.md'
import EinkContent from './eink.md'
import SkillsContent from './skills.md'
import PassionsContent from './passions.md'

@Radium
class CV extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: color.sky,
        secondary: color.blue
      }
    }
    document.body.style.backgroundColor = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
  }

  render() {
    return (
      <div>
        <Hero headline='Curriculum Vitae' />
        <Container>
          <Column width='half' headline='A Full Stack Developer with a keen sense of Design'>
            <Anchor url='../media/docs/peter-tait-cv.pdf' text='Download the PDF' margin='topBottom' />
          </Column>
          <Column width='half'>
            <div style={styles.content} dangerouslySetInnerHTML={{ __html: IntroContent }} />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column width='full' headline='Work' />
        </Container>
        <Container>
          <Column width='half'>
            <h3 style={styles.h3}>DWA</h3>
            <h4 style={styles.h4}>Sept 2015–Present</h4>
          </Column>
          <Column width='half'>
            <h3 style={styles.h3}>Full Stack Developer</h3>
            <div style={styles.content} dangerouslySetInnerHTML={{ __html: FreelanceContent }} />
          </Column>
        </Container>
        <Container>
          <Column width='half'>
            <h3 style={styles.h3}>Freelance</h3>
            <h4 style={styles.h4}>July 2013–Sept 2015</h4>
          </Column>
          <Column width='half'>
            <h3 style={styles.h3}>Web Developer & UI/UX Designer</h3>
            <div style={styles.content} dangerouslySetInnerHTML={{ __html: PebbleContent }} />
          </Column>
        </Container>
        <Container>
          <Column width='half'>
            <h3 style={styles.h3}>Eidola Records</h3>
            <h4 style={styles.h4}>April 2005– 2017 <br/></h4>
          </Column>
          <Column width='half'>
            <h3 style={styles.h3}>Web Developer/ Designer</h3>
            <div style={styles.content} dangerouslySetInnerHTML={{ __html: EinkContent }} />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column width='full' headline='Personal' />
        </Container>
        <Container>
          <Column width='half'>
            <h3 style={styles.h3}>Skills</h3>
            <div style={styles.content} dangerouslySetInnerHTML={{ __html: SkillsContent }} />
          </Column>
          <Column width='half'>
            <h3 style={styles.h3}>Passions</h3>
            <div style={styles.content} dangerouslySetInnerHTML={{ __html: PassionsContent }} />
          </Column>
        </Container>
      </div>
    )
  }
}

const styles = {
  h3: {
    fontSize: type.gamma,
    fontFamily: type.fontSans,
    marginBottom: '5px'
  },
  h4: {
    fontSize: type.delta,
    fontWeight: type.fontRegular,
    opacity: '0.5',
    marginBottom: '20px'
  },
  h5: {
    marginTop: '-15px',
    fontWeight: type.fontRegular
  },
  content: {
    fontSize: type.delta
  },
  education: {
    marginBottom: '30px'
  },
  award: {
    marginTop: '5px'
  }
}

export default CV

import React, { Component, PropTypes } from 'react'

import { color } from '../../../vars'

import HeroFeature from '../../../components/HeroFeature'
import Container from '../../../components/Container'
import Column from '../../../components/Column'
import Block from '../../../components/Block'
import List from '../../../components/List'
import BlockGrid from '../../../components/BlockGrid'
import ClientLogo from '../../../components/ClientLogo'
import Media from '../../../components/Media'
import Anchor from '../../../components/Anchor'

import IntroContent from './intro.md'
import APMContent from './apm.md'
import TemplarsContent from './templars.md'
import TraditionContent from './tradition.md'

class Project6 extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: color.websites,
        secondary: color.white
      }
    }
    document.body.style.backgroundColor = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
  }

  render() {
    const involvement = [
      { name: 'Lorem ipsum dolor sit amet' },
      { name: 'Lorem ipsum dolor sit amet' },
      { name: 'Lorem ipsum dolor sit amet' }
    ]
    return (
      <div>
        <HeroFeature headline='Lorem ipsum dolor sit amet' image='project6' color={this.state.theme.primary} />
        <Container>
          <Column width='third'/>
          <Column
            width='twoThird'
            intro='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
            content={IntroContent}>
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='APM Technologies'>
            <Anchor url='http://apmtechnologies.com' text='Visit the site' margin='topBottom' />
          </Column>
          <Column
            width='twoThird'
            content={APMContent} />
          <Column width='full'>
            <Media
              media='image'
              position='marginBottomNegative'
              url='../images/work/websites/apm.png'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet'>
            <Anchor url='http://google.com' text='Visit the site' margin='topBottom' />
          </Column>
          <Column
            width='twoThird'
            content={TemplarsContent} />
          <Column width='full'>
            <Media
              media='image'
              position='marginBottomNegative'
              url='../images/work/websites/templars.png'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet'>
            <Anchor url='http://g.co.uooglek' text='Visit the site' margin='topBottom' />
          </Column>
          <Column
            width='twoThird'
            content={TraditionContent} />
          <Column width='full'>
            <Media
              media='image'
              position='marginBottomNegative'
              url='../images/work/websites/tradition.png'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='full'
            headline='Related projects'>
            <BlockGrid
              title='Lorem ipsum dolor sit amet'
              client='Lorem ipsum dolor sit amet'
              url='/work/customer-satisfaction'
              path='express' />
            <BlockGrid
              title="Lorem ipsum dolor sit amet"
              client='Lorem ipsum dolor sit amet'
              url='/work/pebble'
              path='pebble' />
            <BlockGrid
              title='Lorem ipsum dolor sit amet'
              client='Lorem ipsum dolor sit amet'
              url='/work/remote-education'
              path='education' />
          </Column>
        </Container>
      </div>
    )
  }
}

export default Project6

import React, { Component, PropTypes } from 'react'

import { color } from '../../../vars'

import HeroFeature from '../../../components/HeroFeature'
import Container from '../../../components/Container'
import Column from '../../../components/Column'
import ClientLogo from '../../../components/ClientLogo'
import Block from '../../../components/Block'
import List from '../../../components/List'
import BlockGrid from '../../../components/BlockGrid'
import Media from '../../../components/Media'
import Button from '../../../components/Button'
import Process from '../../../components/Process'

import IntroContent from './intro.md'
import ProductContent from './product.md'
import VideoContent from './video.md'
import FasttrackContent from './fasttrack.md'

class Project1 extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: color.education,
        secondary: color.white
      }
    }
    document.body.style.backgroundColor = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
  }

  play() {
    const audio = document.getElementById('audio')
    audio.play()
  }

  render() {
    const involvement = [
      { name: 'Workflows + product mapping' },
      { name: 'Wireframing' },
      { name: 'Art + film direction' },
      { name: 'Lead design' },
      { name: 'Illustration' },
      { name: 'Lead frontend development' },
      { name: 'Responsive webapp design' }
    ]
    const processDiscover = [
      { name: 'Client workshops' },
      { name: 'Current market alternatives' }
    ]
    const processAnalyse = [
      { name: 'Team workshop' },
      { name: 'Product map' },
      { name: 'Concepts' }
    ]
    const processDevelop = [
      { name: 'Wireframes' },
      { name: 'Hi-fi designs' },
      { name: 'Prototypes + MVP' },
      { name: 'Develop product with continuous releases' }
    ]
    const processOutcome = [
      { name: 'Product released' },
      { name: 'Handover + training' }
    ]
    return (
      <div>
        <HeroFeature headline='HeroFeature headline' image='project1' color={this.state.theme.primary} />
        <Container>
          <Column width='third'>
            <ClientLogo client='' />
          </Column>
          <Column
            width='twoThird'
            intro='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
            content={IntroContent}>
            <Block
              border='solid'
              title='Involvement'
              intro='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'>
              <List items={involvement} />
            </Block>
          </Column>
        </Container>
        <hr />
        <Container>
          <Column width='full'>
            <Block
              type='process'
              border='solid'
              title='Process'>
              <Process
                title='Discover'
                items={processDiscover} />
              <Process
                title='Analyse'
                items={processAnalyse} />
              <Process
                title='Develop'
                items={processDevelop} />
              <Process
                title='Outcome'
                items={processOutcome} />
            </Block>
          </Column>
          <Column
            width='third'
            headline='Brief: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' />
          <Column
            width='twoThird'
            content={ProductContent} />
          <Column width='full'>
            <Media
              media='image'
              position='marginBottomNegative'
              url='../images/work/education/devices.png'
              alt='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' />
          </Column>
        </Container>
        <hr />
        <Media
          media='video'
          url='../media/work/education/demo.mp4'
          type='background'
          gradient='true'
          color={this.state.theme.primary} />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' />
          <Column
            width='twoThird'
            content={FasttrackContent}>
          </Column>
          <Column width='full'>
            <Media
              url='../images/work/education/devices-responsive.png'
              alt='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' />
          <Column
            width='twoThird'
            content={VideoContent}>
            <Button
              click={this.play.bind(this)}
              text='Play Sample Voiceover'
              icon='play'
              color='education' />
            <audio id='audio' preload='auto'>
              <source src='../media/work/education/voiceover.mp3' type='audio/mp3' />
              <source src='../media/work/education/voiceover.wav' type='audio/wav' />
            </audio>
          </Column>
          <Column width='full'>
            <Media
              media='image'
              url='../images/work/education/devices-video.png'
              alt='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='full'
            headline='Related projects'>
            <BlockGrid
              title='Lorem ipsum dolor sit amett'
              client='Lorem ipsum dolor sit amet'
              url='/work/nucleus'
              path='nucleus' />
            <BlockGrid
              title='Lorem ipsum dolor sit amet'
              client='Side Project'
              url='/work/metrik'
              path='metrik' />
            <BlockGrid
              title='Lorem ipsum dolor sit ametp'
              client='Lorem ipsum dolor sit amet'
              url='/work/people-search'
              path='people' />
          </Column>
        </Container>
      </div>
    )
  }
}

export default Project1

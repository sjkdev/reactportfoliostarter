import React, { Component, PropTypes } from 'react'

import { color } from '../../../vars'

import HeroFeature from '../../../components/HeroFeature'
import Container from '../../../components/Container'
import Column from '../../../components/Column'
import Block from '../../../components/Block'
import List from '../../../components/List'
import BlockGrid from '../../../components/BlockGrid'
import Media from '../../../components/Media'
import Anchor from '../../../components/Anchor'
import Button from '../../../components/Button'

import IntroContent from './intro.md'
import PatientContent from './patient.md'
import BeaconContent from './beacon.md'
import PokeyContent from './pokey.md'

class Project2 extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: color.experiments,
        secondary: color.black
      }
    }
    document.body.style.backgroundColor = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
  }

  render() {
    const patientInvolvement = [
      { name: 'Lead design' },
      { name: 'Frontend development' },
      { name: 'Built with React + Cordova' },
      { name: 'Hackathon winner' }
    ]
    return (
      <div>
        <HeroFeature headline='Lorem ipsum dolor sit amet' image='project2' color={this.state.theme.primary} />
        <Container>
          <Column width='third'>
            <Anchor url='https://github.com/kingsj' text='Github Profile' type='block' color='experiments' margin='bottom' />
          </Column>
          <Column
            width='twoThird'
            intro='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
            content={IntroContent}>
          </Column>
        </Container>
        <hr />
        <Media
          media='image'
          url='../images/work/experiments/feature-patient.png'
          type='background'
          gradient='true'
          color={this.state.theme.primary} />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet' />
          <Column
            width='twoThird'
            content={PatientContent}>
            <Block title='Involvement' color='black' border='solid'>
              <List color='black' items={patientInvolvement} />
            </Block>
          </Column>
          <Column width='full'>
            <Media
              media='image'
              url='../images/work/experiments/devices-patient.png'
              position='marginBottomNegative'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Curling with iBeacons' />
          <Column
            width='twoThird'
            content={BeaconContent}>
            <Anchor url='https://github.com/kingsj' text='See source on Github' margin='topBottom' />
            <Media
              media='image'
              url='../images/work/experiments/devices-beacon.png'
              position='marginBottomNegative'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Pokey' >
            <Button
            text='Download coming soon'
            icon='download'
            color='experiments'
            margin='bottom'
            type='block' />
          </Column>
          <Column
            width='twoThird'
            content={PokeyContent}>
            <Media
              media='image'
              url='../images/work/experiments/pokey.svg'
              type='border'
              color='black'
              alt='Pokey sample' />
            <Media
              media='image'
              url='../images/work/experiments/pokey-abc.svg'
              type='border'
              color='black'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='full'
            headline='Related projects'>
            <BlockGrid
              title="Lorem ipsum dolor sit amet"
              client='Lorem ipsum dolor sit amet'
              url='/work/pebble'
              path='pebble' />
            <BlockGrid
              title='Lorem ipsum dolor sit amet'
              client='Side Project'
              url='/work/metrik'
              path='metrik' />
            <BlockGrid
              title="Lorem ipsum dolor sit amet"
              client='Various Clients'
              url='/work/marketing-websites'
              path='websites' />
          </Column>
        </Container>
      </div>
    )
  }
}

export default Project2

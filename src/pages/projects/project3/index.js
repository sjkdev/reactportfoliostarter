import React, { Component, PropTypes } from 'react'

import { color } from '../../../vars'

import HeroFeature from '../../../components/HeroFeature'
import Container from '../../../components/Container'
import Column from '../../../components/Column'
import Block from '../../../components/Block'
import List from '../../../components/List'
import BlockGrid from '../../../components/BlockGrid'
import Media from '../../../components/Media'
import Vimeo from '../../../components/Vimeo'
import Button from '../../../components/Button'

import IntroContent from './intro.md'
import LearnContent from './learn.md'

class Project3 extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: color.metrik,
        secondary: color.white
      }
    }
    document.body.style.backgroundColor = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
  }

  render() {
    const involvement = [
      { name: 'Lorem ipsum dolor sit amet' },
      { name: 'Lorem ipsum dolor sit amet' },
      { name: 'Lorem ipsum dolor sit amet' }
    ]
    return (
      <div>
        <HeroFeature headline='HeroFeature headline' image='project3' color={this.state.theme.primary}/>
        <Container>
          <Column width='third'>
            <Button text='Download coming soon' icon='download' color='metrik' />
          </Column>
          <Column
            width='twoThird'
            intro='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
            content={IntroContent}>
            <Block
              border='solid'
              title='Involvement'
              intro='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'>
              <List items={involvement} />
            </Block>
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Logo sample.' />
          <Column width='twoThird'>
            <Media
              media='image'
              url='../images/work/metrik/logo.svg'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet' />
          <Column width='twoThird'>
            <Media
              media='image'
              url='../images/work/metrik/abc.svg'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Tester sample.' />
          <Column width='twoThird'>
            <Media
              media='image'
              url='../images/work/metrik/text.svg'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Media
          media='image'
          url='../images/work/metrik/book.jpg'
          type='background'
          gradient='true'
          color={this.state.theme.primary} />
        <Media
          media='image'
          url='../images/work/metrik/shapes.svg'
          alt='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' />
        <Container>
          <Column
            width='third'
            headline='Learning by doing.' />
          <Column
            width='twoThird'
            content={LearnContent}>
            <Vimeo url='https://player.vimeo.com/' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='full'
            headline='Related projects'>
            <BlockGrid
              title="Lorem ipsum dolor sit amet"
              client='pebble {code}'
              url='/work/pebble'
              path='pebble' />
            <BlockGrid
              title='Lorem ipsum dolor sit amet'
              client='Lorem ipsum dolor sit amet'
              url='/work/nucleus'
              path='nucleus' />
            <BlockGrid
              title='Lorem ipsum dolor sit amet'
              client='Lorem ipsum dolor sit amet'
              url='/work/remote-education'
              path='education' />
          </Column>
        </Container>
      </div>
    )
  }
}

export default Project3

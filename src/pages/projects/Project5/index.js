import React, { Component, PropTypes } from 'react'

import { color } from '../../../vars'

import HeroFeature from '../../../components/HeroFeature'
import Container from '../../../components/Container'
import Column from '../../../components/Column'
import Block from '../../../components/Block'
import List from '../../../components/List'
import BlockGrid from '../../../components/BlockGrid'
import ClientLogo from '../../../components/ClientLogo'
import Media from '../../../components/Media'
import Button from '../../../components/Button'
import Process from '../../../components/Process'

import IntroContent from './intro.md'
import DesignContent from './design.md'
import ResultsContent from './results.md'
import PerformanceContent from './performance.md'
import IllustrationsContent from './illustrations.md'

class Project5 extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: color.pebble,
        secondary: color.white
      }
    }
    document.body.style.backgroundColor = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
  }

  render() {
    const involvement = [
      { name: 'Lead design' },
      { name: 'Development of Jekyll site' },
      { name: 'Brand illustrations' }
    ]
    const processDiscover = [
      { name: 'Internal workshops' },
      { name: 'Current sitemap' },
      { name: 'Current userflow' },
      { name: 'Understand business requirements' }
    ]
    const processAnalyse = [
      { name: 'Product map' },
      { name: 'Concepts' },
      { name: 'Technology experiments' }
    ]
    const processDevelop = [
      { name: 'Wireframes' },
      { name: 'Hi-fi designs' },
      { name: 'Prototypes' },
      { name: 'Develop website with continuous releases' }
    ]
    const processOutcome = [
      { name: 'Continuous releases' },
      { name: 'Increase of up to 666% traffic' }
    ]
    return (
      <div>
        <HeroFeature headline='Lorem ipsum dolor sit amet' image='project5' color={this.state.theme.primary} />
        <Container>
          <Column width='third'>
            <ClientLogo client='pebble' />
          </Column>
          <Column
            width='twoThird'
            intro='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
            content={IntroContent}>
            <Block
              border='solid'
              title='Involvement'
              intro='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'>
              <List items={involvement} />
            </Block>
          </Column>
        </Container>
        <hr />
        <Container>
          <Column width='full'>
            <Block
              type='process'
              border='solid'
              title='Process'>
              <Process
                title='Discover'
                items={processDiscover} />
              <Process
                title='Analyse'
                items={processAnalyse} />
              <Process
                title='Develop'
                items={processDevelop} />
              <Process
                title='Outcome'
                items={processOutcome} />
            </Block>
          </Column>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet' />
          <Column
            width='twoThird'
            content={DesignContent} />
          <Column width='full'>
            <Media
              media='image'
              position='marginBottomNegative'
              url='../images/work/pebble/browser.png'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet' />
          <Column
            width='twoThird'
            content={ResultsContent} />
          <Column width='full'>
            <Media
              media='image'
              position='marginBottomNegative'
              url='../images/work/pebble/404.png'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet' />
          <Column
            width='twoThird'
            content={PerformanceContent} />
          <Column width='full'>
            <Media
              media='image'
              position='marginBottomNegative'
              url='../images/work/pebble/devices.png'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='third'
            headline='Lorem ipsum dolor sit amet' />
          <Column
            width='twoThird'
            content={IllustrationsContent} />
          <Column width='full'>
            <Media
              media='image'
              url='../images/work/pebble/illustrations.png'
              alt='Lorem ipsum dolor sit amet' />
          </Column>
        </Container>
        <hr />
        <Container>
          <Column
            width='full'
            headline='Related projects'>
            <BlockGrid
              title='Lorem ipsum dolor sit amet'
              client='Lorem ipsum dolor sit amet'
              url='/work/customer-satisfaction'
              path='express' />
            <BlockGrid
              title='Lorem ipsum dolor sit amet'
              client='Lorem ipsum dolor sit amet'
              url='/work/metrik'
              path='metrik' />
            <BlockGrid
              title="Lorem ipsum dolor sit amet"
              client='Various Clients'
              url='/work/marketing-websites'
              path='websites' />
          </Column>
        </Container>
      </div>
    )
  }
}

export default Project5

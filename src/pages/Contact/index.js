import React, { Component, PropTypes } from 'react'

// import { color } from '../../vars'

import { bgImg } from '../../vars'

import Hero from '../../components/Hero'
import Container from '../../components/Container'
import Column from '../../components/Column'
import ContactForm from '../../components/ContactForm'
import Anchor from '../../components/Anchor'


class Contact extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: bgImg.bg1,
        secondary: bgImg.bg2
      }
    }
    document.body.style.backgroundImage = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
    // document.body.style.backgroundColor = "red";
    document.body.style.backgroundColor = '#fff'
    document.body.style.color = '#222'
  }

  render() {
    return (
      <div>
        <Hero headline='Talk to me.' />
          <Container>
            <Column
              width='half'
              title='Get in touch and say hello. Whether you have a work enquiry or simply want to find out more, let’s talk.'>
              <Anchor url='mailto:contact@helloseanking.co.uk' text='Send me an email' margin='topBottom' />
            </Column>
            <Column width='half'>
              <ContactForm color='blue' />
            </Column>
          </Container>
      </div>
    )
  }
}

export default Contact

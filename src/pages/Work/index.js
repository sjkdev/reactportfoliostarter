import React, { Component, PropTypes } from 'react'

// import { color } from '../../vars'

import { bgImg } from '../../vars'

import Hero from '../../components/Hero'
import Container from '../../components/Container'
import Column from '../../components/Column'
import BlockGrid from '../../components/BlockGrid'
import ListItem from '../../components/ListItem'
import AnchorLink from '../../components/AnchorLink'

class Work extends Component {
  componentWillMount() {
    this.state = {
      theme: {
        primary: bgImg.bg1,
        secondary: bgImg.bg2
      }
    }
    document.body.style.backgroundImage = this.state.theme.primary
    document.body.style.color = this.state.theme.secondary
  }

  render() {
    return (
      <div>
        <Hero headline='Selected Work.' />
        <div>
        <Column width='full'>
        
        </Column>
        </div>
        <Container>
          <Column width='full'>
          <BlockGrid
              title='Github'
              client='Github'
              url='/https://github.com/seanjamesking'
              target='_blank'
              path='project2' />
            <BlockGrid
              title='BitBucket'
              client='Bitbucket'
              url='https://bitbucket.org/sjkdev'
              target='_blank'
              path='project3' />

           {/* <BlockGrid
              title='Project1'
              client='Project1'
              url='/work/Project1'
              path='project1' />
            <BlockGrid
              title='Project2'
              client='Project2'
              url='/work/Project2'
              path='project2' />
            <BlockGrid
              title='Project3'
              client='Project3'
              url='/work/Project3'
              path='project3' />
            <BlockGrid
              title='Project4'
              client='Project4'
              url='/work/Project4'
              path='project4' />
            <BlockGrid
              title='Project5'
              client='Project5'
              url='/work/Project5'
              path='project5' />
            <BlockGrid
              title='Project6'
              client='Project6'
              url='/work/Project6'
              path='project6' />
            <BlockGrid
              title='Project7'
              client='Project7'
              url='/work/Project7'
              path='project7' />
            <BlockGrid
              title='Project8'
              client='Project8'
              url='/work/Project8' 
              path='project8' /> */}
          </Column>
          <hr />
      <Container>
        <Column
          width='full'
          title='Selected Work'>
          <ListItem
            title='React Dev builds'
            // summary='Minim efficiendi in has, et est dicit nostro referrentur.'
            link='work/nucleus' />
          <ListItem
            title='Custom WP Starter'
            // summary='Minim efficiendi in has, et est dicit nostro referrentur.' 
            link='work/remote-education' />
          <ListItem
            title='Laravel Dev builds'
            // summary='Minim efficiendi in has, et est dicit nostro referrentur.' 
            link='work/people-search' />
          <ListItem
            title='Angular Dev builds'
            // summary='Minim efficiendi in has, et est dicit nostro referrentur.'
            link='/work/metrik' />
          <ListItem
            title='AI/ Python Dev builds'
            // summary='Minim efficiendi in has, et est dicit nostro referrentur.'
            link='work/customer-satisfaction' />
          
          <ListItem
            title='Django Dev builds'
            // summary='Minim efficiendi in has, et est dicit nostro referrentur.'
            link='/work/pebble' />
            <AnchorLink link='/work' text='View all work' margin='top' />
        </Column>
      </Container>
        </Container>
      </div>
    )
  }
}

export default Work

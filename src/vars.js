// Colours
export const color = {
  white: '#fff',
  black: '#151820',
  blue: '#0032D2',
  sky: '#e5eeff',
  ash:'#f5f5f5',
  nucleus: '#3d1d90',
  // education: '#00A9C6',
  education: '#999',
  people: '#C90077',
  metrik: '#00bc6c',
  express: '#252678',
  pebble: '#D94774',
  agile: '#eed900',
  experiments: '#ffc800',
  websites: '#656a77'
}


// background img
export const bgImg ={
  bg1: '../build/images/Background.png',
  bg2: '../build/images/Background-purpe-smoke.png',
  bg3: '../build/images/Background3.png',
  bg4: '../build/images/Backgroundpurp-lightblue.png'
}

// Typography
export const type = {
  tera: '84px',
  giga: '62px',
  mega: '48px',
  alpha: '38px',
  beta: '26px',
  gamma: '20px',
  delta: '16px',
  epsilon: '14px',
  zeta: '12px',

  fontRegular: 'normal',
  fontBold: 'bold',
  fontMono: 'roboto, monospace',
  fontSans: 'Open Sans, sans-serif',

  bodySize: '18px',
  bodyWeight: 'normal'
}

// Layout
export const layout = {
  gutter: '20px',
  maxWidth: '1250px'
}

// Breakpoints
export const breakpoint = {
  large: '@media screen and (min-width: 1250px)',
  medium: '@media screen and (min-width: 800px)',
  small: '@media screen and (min-width: 600px)',
}

const vars = {
  color,
  type,
  layout,
  breakpoint
}

export default vars

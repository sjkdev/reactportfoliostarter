# React Starter template for personal website/blog.

<br>

## Tech used:

- Babel 6
- Webpack
- React
- Radium inline styles

<br>

## Getting Started

```sh
npm install
```

Start the local dev server:

```sh
npm start
```

Navigate to **http://localhost:8085/** to view the app.

<br>

## Build for Production

```sh
npm run build
```

<br>

## Clean

```sh
npm run clean
```

<br>

## Deploy to Netlify

```sh
npm run deploy
```

<br>

## Notes
```sh
default colouring in portfolio section deliberately gross to enforce some nice colour schemes to be implemented
```
